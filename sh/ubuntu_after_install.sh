#/bin/bash

echo "aptitude"

sudo apt-get install -y aptitude

echo "Installing git"

sudo aptitude install -y git

echo "medias and extras"

sudo aptitude install -y ubuntu-restricted-extras vlc gnome-tweak

echo "helpers"

sudo aptitude install -y synaptic

echo "dev tools"

sudo aptitude install -y httpie
